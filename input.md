* alias user1: User 1
* alias user2: User 2
* alias user3: User 3

* group $all: user1 user2 user3

# Team meetings
+ 2018-12-01 12:00-14:00 $all: Team meeting
+ 2019-01-12 14:00-16:00 user1 user3: Team meeting without User 2

# Collaborative sessions
+ 2018-10-19 17:00-20:05 user1 user2: User 1 and User 2 worked together

# User 1
+ 2018-09-12 12:00-09:00 user1 user2: User 1 worked

# User 2
+ 2018-09-12 23:00-03:00 user2: User 2 worked

# User 3
+ 2018-11-12 14:34-19:00 user3: User 3 worked
