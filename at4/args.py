import argparse
from at4 import run
from at4 import init

args = None

def parse_args():
    global args
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=parser.print_help)
    parser.add_argument('-p', '--profile', help='Configuration profile', default='DEFAULT')

    subparsers = parser.add_subparsers(title='commands')

    parser_run = subparsers.add_parser('run', help='Run at4')
    parser_run.set_defaults(func=run.main)

    parser_init = subparsers.add_parser('init', help='Create a .at4 directory')
    parser_init.set_defaults(func=init.main)

    args = parser.parse_args()