import pkg_resources
import os
import logging

def get_from_dir(path: str):
    if os.path.isfile(path):
        with open(path, 'r') as file:
            return file.read()
    logging.info("Couldn't load %s  from directory." % path)
    return None

def get_from_pkg(path: str):
    if (pkg_resources.resource_exists(__name__, path)
            and not pkg_resources.resource_isdir(__name__, path)):
        return pkg_resources.resource_string(__name__, path).decode('utf8')
    logging.info("Couldn't load %s from package." % path)
    return None

def get(path: str):
    file_from_dir = get_from_dir(path)
    if (file_from_dir is not None):
        return file_from_dir
    else:
        file_from_pkg = get_from_pkg(path)
        if (file_from_pkg is not None):
            logging.info("'%s' could not be found, so the default value is used."
                % path)
            logging.info("Use 'at4 init' to create the files.")
            return file_from_pkg

def copy_from_pkg(src, dest, overwrite=False):
    if pkg_resources.resource_isdir(__name__, src):
        if not os.path.isdir(dest):
            logging.info("Creating directory '%s'" % dest)
            os.mkdir(dest)
        for entry in pkg_resources.resource_listdir(__name__, src):
            copy_from_pkg(os.path.join(src, entry), os.path.join(dest, entry))
    else:
        if not os.path.isfile(dest):
            logging.info("Creating file '%s'" % dest)
            with pkg_resources.resource_stream(__name__, src) as src_file:
                open(dest, 'wb').write(src_file.read())