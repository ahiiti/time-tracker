import configparser
import pkg_resources
from at4 import args
import logging

path = '.at4/config'

configuration = configparser.ConfigParser()

def get(key: str):
    profile = configuration[args.args.profile]
    return profile[key]

def get_config():
    return configuration[args.args.profile]

def read_config():
    configuration.read_string(pkg_resources.resource_string(__name__, path).decode('utf8'))
    try:
        with open(path, 'r') as configfile:
            configuration.read_file(configfile)
    except:
        logging.info("'%s' could not be opened. Use 'at4 init' to create." % path)

    logging.info(configuration.sections())