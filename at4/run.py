import json
import os
import io
import re
import datetime
import logging
from at4 import config
from at4 import resources

def main():

    try:
        from jinja2 import Template
    except:
        logging.error('jinja2 could not be imported. Exiting...')
        exit()


    class Record:
        """A Record object represents a time period when someone was engaged in a activity.
        It consists of:
        - a start time
        - an end time
        - name of the engaged person
        - a description of the activity

        Comparing two Record objects is like comparing the start time of both records.
        """
        def __init__(self, year, month, day, start_hour, start_minute, end_hour, end_minute, name, description):
            self.start = datetime.datetime(year, month, day, start_hour, start_minute)
            self.end = datetime.datetime(year, month, day, end_hour, end_minute)
            
            if self.start > self.end:
                self.end += datetime.timedelta(days=1)
            
            self.name = name
            self.description = description

        def duration_minutes(self) -> int:
            """Returns the duration in minutes.
            """
            return (self.end-self.start).total_seconds() / 60
        
        def duration(self) -> (int, int):
            """Returns the duration as a (hours, minutes) tuple.
            """
            return minutes_to_tuple(self.duration_minutes())

        # methods for python's built-in comparisons:
        def __lt__(self, other):
            return self.start.__lt__(other.start)
        def __gt__(self, other):
            return self.start.__gt__(other.start)



    # -------------- program

    # the input file, seperated by lines
    inputLines = None
    # the names in the form {"alias": "Display name", ...}
    names = {}
    # the groups in the form {"group name": {"member1", "member2", ...}, ...}
    groups = {}
    # the records in the form
    records = []

    # The set of lines that have been interpreted.
    # Later, the difference of interpreted and all line numbers will be printed.
    # If the user has made a syntactical mistake, he will
    # be informed about which lines were not interpreted.
    interpreted_line_numbers = set()

    # read lines in file to list
    with open(config.get('input'), mode='r') as input_file:
        inputLines = input_file.read().splitlines()

    def match_all(pattern: str, lines: [str]) -> (int, str, object):
        """Returns a list of (line number, original line, regex matchings)
        for each line from lines that matches the regex pattern.
        """
        return [(n, lines[n], re.match(pattern, lines[n]).groups())
                for n in range(len(lines))
                if re.match(pattern, lines[n]) != None]

    # handle aliases
    pattern_alias = r'^\s*\*\s*alias\s+([^:\s]*)\s*:\s*([^\s].*[^\s])\s*$'
    for (n, line, re_groups) in match_all(pattern_alias, inputLines):
        alias, name = re_groups
        names[alias] = name
        interpreted_line_numbers.add(n)

    # handle groups
    pattern_group = r'^\s*\*\s*group\s+([^:\s]*)\s*:(.*)$'
    for (n, line, re_groups) in match_all(pattern_group, inputLines):
        group_name, members = re_groups
        members = set(members.split())
        groups[group_name] = members
        interpreted_line_numbers.add(n)

    # handle records
    pattern_record = r'^\s*\+\s*(\d+)-(\d+)-(\d+)\s+(\d+):(\d+)-(\d+):(\d+)\s+([^:]*):(.*)$'
    for (n, line, re_groups) in match_all(pattern_record, inputLines):
        year, month, day, start_hour, start_minutes, end_hour, end_minutes, people, description = re_groups
        year, month, day, start_hour, start_minutes, end_hour, end_minutes = \
            int(year), int(month), int(day), int(start_hour), int(start_minutes), int(end_hour), int(end_minutes)
        # extract people
        related_groups = [group for group in people.split() if group in groups]
        people = {name for name in people.split() if name not in groups}
        [[people.add(name) for name in groups[group]] for group in related_groups]
        # remove leading and tailing spaces
        description = description.strip()

        for name in people:
            record = Record(year, month, day, start_hour, start_minutes, end_hour, end_minutes, name, description)
            records.append(record)
        interpreted_line_numbers.add(n)

    # Print all non-empty and non-whitespace lines that have not been interpreted.
    for n in range(len(inputLines)):
        if n not in interpreted_line_numbers and inputLines[n].strip():
            logging.info('Line was not interpreted: [%3.i] %s' % (n, inputLines[n]))

    def group_by_lambda(record_list: [Record], lambda_function: lambda Record:object) -> [(object, [Record])]:
        """Returns the records grouped by an arbitrary property using a lambda function.
        For example, using 'lambda record:record.start.day' will return a list that has a tuple (day, [records of this day]).
        The list of tuples will be sorted by their first attribute, if possible.
        The record lists are sorted by date and time.
        """
        values = set(map(lambda_function, record_list))
        grouped = [(value, [record for record in record_list if lambda_function(record) == value]) for value in values]
        try:
            grouped.sort()
        except:
            logging.debug('Grouped record list could not be sorted!')
        return grouped

    def group_by_year(record_list: [Record]) -> [(int, [Record])]:
        """Returns the records grouped by their year.
        The format is [(year, [records from this year])].
        The Tuples will be sorted by year and each record list will be sorted by date, too.
        """
        return group_by_lambda(record_list, lambda record:record.start.year)

    def group_by_month(record_list: [Record]) -> [(int, [Record])]:
        """Returns the records grouped by their month.
        You may want to group by year first!
        The format is [(month, [records from this month])].
        The Tuples will be sorted by month and each record list will be sorted by date, too.
        """
        return group_by_lambda(record_list, lambda record:record.start.month)

    def group_by_week(record_list: [Record]) -> [(int, [Record])]:
        """Returns the records grouped by their calendar week.
        You may want to group by year first!
        The format is [(week, [records from this week])].
        The Tuples will be sorted by week and each record list will be sorted by date, too.
        """
        return group_by_lambda(record_list, lambda record:record.start.isocalendar[1])

    def group_by_day(record_list: [Record]) -> [(int, [Record])]:
        """Returns the records grouped by their day of month.
        You may want to group by month first!
        The format is [(day, [records from this day])].
        The Tuples will be sorted by week and each record list will be sorted by date, too.
        """

    def group_by_week_day(record_list: [Record]) -> [(int, [Record])]:
        """Returns the records grouped by their day of week.
        You may want to group by week first!
        The format is [(weekday, [records from this weekday])].
        The Tuples will be sorted by week and each record list will be sorted by date, too.
        """
        return group_by_lambda(record_list, lambda record:record.start.weekday)

    def group_by_name(record_list: [Record]) -> [(str, [Record])]:
        """Returns the records grouped by the name of the engaged person.
        The format is [(name, [records of this person])].
        The Tuples will be sorted alphabetically by name and each record list will be sorted by date, too.
        """
        return group_by_lambda(record_list, lambda record:record.name)

    def sum_time_minutes(record_list: [Record]) -> int:
        """Returns the sum of all record's durations in minutes.
        """
        return int(sum(map(lambda record:record.duration_minutes(), record_list)))

    def sum_time(record_list: [Record]) -> (int, int):
        """Returns the sum of all record's durations as a tuple in (hours, minutes).
        """
        return minutes_to_tuple(sum_time_minutes(record_list))

    def minutes_to_tuple(minutes: int) -> (int, int):
        """Converts minutes to a tuple in (hours, minutes).
        """
        return (minutes // 60, (minutes % 60))

    # generate output
    template_string = resources.get(config.get('template'))
    template = Template(template_string, line_statement_prefix='#')
    result = template.render(
        names=names,
        records=records,
        group_by_lambda=group_by_lambda,
        group_by_year=group_by_year,
        group_by_month=group_by_month,
        group_by_week=group_by_week,
        group_by_week_day=group_by_week_day,
        group_by_name=group_by_name,
        sum_time=sum_time,
        sum_time_minutes=sum_time_minutes)

    # generate output
    with open(config.get('output'), 'w') as output:
        output.write(result)

    # execute the post hook, if configured
    if config.get('post_hook'):
        logging.info('Executing post-hook')
        os.system(config.get('post_hook'))
    else:
        logging.info('No post-hook.')
