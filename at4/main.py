import argparse
from at4 import init
from at4 import run
from at4 import args
import logging
from at4 import config
from colorama import Fore, Back, Style

logo = \
       Back.WHITE \
     + "     "  + Back.RED \
     + "                                        " + Back.RESET + "\n" \
     + Back.WHITE + Fore.BLACK + Style.BRIGHT \
     + " at4 " + Style.RESET_ALL + Back.RED + Fore.WHITE \
     + " ahiiti's truly tremendous time tracker " + Back.RESET + "\n" \
     + Back.WHITE \
     + "     "  + Back.RED \
     + "                                        " + Style.RESET_ALL + "\n" \

def main() -> None:
    #setup logging
    logging.basicConfig(format='%(message)s')
    logging.getLogger().setLevel(logging.INFO)
    logging.info(logo)

    #parse arguments
    logging.debug('Parsing args...')
    args.parse_args()
    #read configuration file
    logging.debug('Reading configuration...')
    config.read_config()
    #run the subcommand
    args.args.func()
    