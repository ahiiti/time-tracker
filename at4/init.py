import os
import logging
from at4 import resources


def main():
    """Copy all default files to .at4
    """
    resources.copy_from_pkg('.at4', '.at4')
