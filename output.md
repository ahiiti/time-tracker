Time Tracker
===

user1 (28:05)
===
September 2018 (21:00)
---
+ **`Wed 2018-09-12 12:00-09:00` &rarr;** `21:00` User 1 worked

October 2018 (03:05)
---
+ **`Fri 2018-10-19 17:00-20:05` &rarr;** `03:05` User 1 and User 2 worked together

December 2018 (02:00)
---
+ **`Sat 2018-12-01 12:00-14:00` &rarr;** `02:00` Team meeting

January 2019 (02:00)
---
+ **`Sat 2019-01-12 14:00-16:00` &rarr;** `02:00` Team meeting without User 2

user2 (30:05)
===
September 2018 (25:00)
---
+ **`Wed 2018-09-12 12:00-09:00` &rarr;** `21:00` User 1 worked
+ **`Wed 2018-09-12 23:00-03:00` &rarr;** `04:00` User 2 worked

October 2018 (03:05)
---
+ **`Fri 2018-10-19 17:00-20:05` &rarr;** `03:05` User 1 and User 2 worked together

December 2018 (02:00)
---
+ **`Sat 2018-12-01 12:00-14:00` &rarr;** `02:00` Team meeting

user3 (08:26)
===
November 2018 (04:26)
---
+ **`Mon 2018-11-12 14:34-19:00` &rarr;** `04:26` User 3 worked

December 2018 (02:00)
---
+ **`Sat 2018-12-01 12:00-14:00` &rarr;** `02:00` Team meeting

January 2019 (02:00)
---
+ **`Sat 2019-01-12 14:00-16:00` &rarr;** `02:00` Team meeting without User 2

