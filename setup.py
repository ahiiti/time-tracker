#!/usr/bin/env python3

from setuptools import setup, Command
from distutils.spawn import spawn
import sys


setup(name = 'at4',
    version = '0.0',
    description = "at4 - ahiiti's truly tremendous time tracker",
    long_description = 'A tool to process and evaluate time records.',
    platforms = ["Linux"],
    author="ahiiti",
    url="https://gitlab.com/ahiiti/time-tracker/",
    license="GPL3",
    packages = ['at4'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'at4 = at4.main:main'
        ]
    }
)
