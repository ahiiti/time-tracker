at4 - ahiiti's truly tremendous time tracker
============================================
(it really is, believe me!)



Installation
------------
### Arch Linux

Install `at4` from arch user repository.

### Other distributions
To install the package for your user, use:

    ./setup.py install --user

You can later remove it using

    pip3 uninstall at4

### Requirements

Be sure to have `python3` and the `jinja2` template engine installed.

| Distribution      | Install this                 |
| ----------------- | ---------------------------- |
| **Arch Linux**    | `pacman -S python-jinja`     |
| **Debian/Ubuntu** | `apt install python3-jinja2` |
| **Fedora/CentOS** | `dnf install python3-jinja2` |

First Steps
-----------

Create a file `input.md`...

    + 2019-12-31 23:00-01:00 alice: Celebrate new year

...and run at4:

    at4 run

A file named `output.md` will appear.
This will be rendered nicely on git platforms like GitLab and GitHub.

### More users

Append this to `input.md`:

    + 2019-12-31 23:00-01:00 bob: Celebrate new year

After running `at4 run`, there will be entries for alice and bob.

Let's let alice and bob celebrate together by replacing both entries in `input.md` with:

    + 2019-12-31 23:00-01:00 alice bob: Celebrate together

Now, `at4 run` generates the same output, but we got rid of redundancy.

### Groups

If Alice and Bob share a lot of common time records together, they could define a group:

    * group mygroup: alice bob

    + 2019-12-31 23:00-01:00 mygroup: Celebrate new year
    + 2020-01-13 20:00-03:00 mygroup: Yet another party

### Pretty names

For now, we only used short single-word aliases, and this is fine for our input file.
However, if we want to see more complicated names in our output, we can define an alias:

    * alias alice: Alice van der Horstenteych
    * alias Bob: Bob Andrews

The input format was designed to be compatible with markdown.

The program ignores all lines, except for these exceptions:

  - Commands, which begin with `*`
  - Time records, which begin with `+`

Leading whitespaces are also allowed.

All other lines are ignored and can be used for comments.

### Records
Contains the date, time span, participants and a description of the activity.

    + YYYY-MM-DD HH:MM-HH:MM <participants>: <description>

Example:

    + 2020-02-01 10:00-14:30 alice: Deleted e-mails

### Alias Command
Assign a display name to a name.

    * alias <alias>: <display name>

Example:

    * alias bob: Bob the Builder

### Group Command

    * group <group name>: <member1> <member2> ...

Example:

    * group secretary: alice bob


Output templates
----------------

`time-tracker` uses the jinja2 template engine. You can create templates to adjust the output to your needs.

There are pre-made templates for Markdown, LaTeX and Plain-Text in `templates`.

Please visit 

The following values are passed to the template:
- `records`, the list of all records
- `names`, a set in the form `{'alias': 'Display name'}`
The following functions are available in the template:
- `group_by` functions:
  Group lists of records by various properties, sorted by the same property.
  For instance, group_by_year might return
  `[(2019, [<records from 2019>]), (2020, [<records from 2020>])]`
  - `group_by_name(records)`
  - `group_by_year(records)`
  - `group_by_week(records)`
  - `group_by_month(records)`
  - `group_by_week_day(records)`
  - `group_by_name(records)`
- `group_by_lambda(records, lambda)`:
  Pass a lambda function that takes one record and returns the value to sort and group as you like.

  For example, `lambda record:record.year` will sort and group by the year
- `sum_time_minutes(records)`:

  Returns the sum of the records durations in minutes
- `sum_time(records)`:

  Returns the sum of the records durations in `(<hours>, <minutes>)`
